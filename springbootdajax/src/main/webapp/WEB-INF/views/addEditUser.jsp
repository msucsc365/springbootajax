<!doctype html>
<html lang="en">
<%@ include file="/WEB-INF/layouts/include.jsp" %>
<%@ include file="/WEB-INF/layouts/head.jsp" %>
<body id="demo-body">
	<div id="demo-main-div" class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<h1>${title}</h1>
				<%@ include file="/WEB-INF/layouts/message.jsp" %>
				<%-- 
					The Add/Edit User Form
					We are using ${pageContext.request.contextPath} to reference the context path.
					The order of parsing does not allow us to use <%=request.getContextPath() %>  
				--%>
				<form:form method="post" id="userForm" modelAttribute="command" 
					action="${pageContext.request.contextPath}/users/addEditUser">
					
					<div class="form-group col-sm-3">
						<%--
							The label and input for firstName 
							path="firstName" is converted to: 
								id="firstName" name="firstName" 
							in the HTML 
						 --%>
						<label for="firstName">First Name</label>
						<form:input class="form-control" path="firstName" placeholder="Enter First Name" />
						
						<%--
							The label and input for lastName
						 --%>	
						<label for="lastName" class="mt10">Last Name</label>
						<form:input class="form-control" path="lastName" placeholder="Enter Last Name" />
						
						<%-- 
							The submit button. Notice that the type is "button" and not submit. We do 
							not want to submit the form when the button is clicked. Instead we want to
							validate the form (client-side), and then send the form to the server. 
						--%>
						<button class="btn btn-primary mt-3" type="button" id="submitBtn">
							<img id="spinnerImage" class="d-none" height="16px" src="<c:url value='/resources/img/spinner2.gif' />" />
							Submit
						</button>
						<div id="waitMessage" class="d-none">Please wait...</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
	<style>
		#userForm .form-group {
			padding-left: 0px !important;
		}
	</style>
	<script>
		var btn;
		var spinnerImage;
    	var waitMessage;
	
		// Client Side Validation
		window.addEventListener("DOMContentLoaded", (event) => {
			btn = document.getElementById("submitBtn")
			spinnerImage = document.getElementById("spinnerImage");
	        waitMessage = document.getElementById("waitMessage");
	        
		    setTimeout(function () {
		    }, 2000);
			
			// Get access to submit button element 
			let submitBtn = document.getElementById("submitBtn")
			
			// Handle/Process Submit Button Click
			submitBtn.addEventListener("click", (event) => {
				// Stop old versions of IE from processing the form 
				// even though the button type is button
				event.preventDefault();
				
				// Variables
				let firstName = document.getElementById("firstName").value;
				let lastName = document.getElementById("lastName").value;
				let form = document.getElementById("userForm"); 
				
				if (firstName.length == 0 || lastName.length == 0) {
					alert("Error: Please enter a first and last name.");
				} else {					
					// Instead of using form.submit(); let's make an AJAX call 
					// using "fetch"
					//form.submit();
					
					try {
						// Diable the button and start the spinner
						disableButton();
						
						let data = {};
						data.firstName = document.getElementById("firstName").value;
						data.lastName = document.getElementById("lastName").value;
						let User = JSON.stringify(data);
						//console.log(dataStr);
						
/* 						let url = "${pageContext.request.contextPath}/users/xAddEditUser?firstName=";
						url += document.getElementById("firstName").value;
						url += "&lastName=";
						url += document.getElementById("lastName").value; */

						// Make AJAX call to the server along with the data (user first and last name)
			 			fetch("<c:url value='/users/xAddEditUser' />", {
			 		        method: "POST",
			 		        body: User,
			 		        headers: {
			 		            "Content-Type": "application/json"
			 		        }
			 			}).then(function(response) {
						  	if (response.ok) {
						  		console.log("response.status=", response.status);
						   		return response;
						  	} else {
						  		throw new Error('Internal Error');	
						  	}
						}).then(function(response) {
							return response.text();
						}).then(function (text) {
							enableButton();
							text = ((text==null || text==undefined || text.length==0) ? "Sorry, Internal Error." : text);
							alert(text);
						}).catch(function(error) {
							enableButton();
							console.log('There was a problem with your fetch operation: ', error.message);
						});
						
						
/* 						// Make AJAX call to the server along with the data (user first and last name)
			 			fetch("<c:url value='/users/xAddEditUser' />", {
			 		        method: "POST",
			 		        body: JSON.stringify(dataStr),
			 		        headers: {
			 		            "Content-Type": "application/json"
			 		        }
			 			}).then(function(response) {
						  	if (response.ok) {
						  		console.log("response.status=", response.status);
						   		return response;
						  	} else {
						  		throw new Error('Internal Error');	
						  	}
						}).then(function(response) {
							return response.text();
						}).then(function (text) {
							enableButton();
							text = ((text==null || text==undefined || text.length==0) ? "Sorry, Internal Error." : text);
							alert(text);
						}).catch(function(error) {
							enableButton();
							console.log('There was a problem with your fetch operation: ', error.message);
						}); */
					} catch (err) {
						enableButton();
						alert(err);
					}
				}
			});
		});
		
	 	function disableButton() {
	 		btn.disabled = true;
	        spinnerImage.classList.remove("d-none");
	        waitMessage.classList.remove("d-none");
	        spinnerImage.classList.add("d-inline");
	        waitMessage.classList.add("d-block");
		}
		
		function enableButton() {
			btn.disabled = false;
			spinnerImage.classList.add("d-none");
	        waitMessage.classList.add("d-none");
	        spinnerImage.classList.remove("d-inline");
	        waitMessage.classList.remove("d-block");
		}
		
	</script>
</body>
</html>