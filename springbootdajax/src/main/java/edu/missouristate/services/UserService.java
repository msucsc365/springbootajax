package edu.missouristate.services;

import java.util.List;

import edu.missouristate.model.User;

public interface UserService {
	public List<User> getUsers();
	public void addUser(User user);
}
