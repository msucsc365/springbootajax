package edu.missouristate.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import edu.missouristate.model.User;
import edu.missouristate.util.MSU;

@Repository
public class UserRepository {
	
	@Autowired
	JdbcTemplate template;
	
    public UserRepository() {
    }    
    
	public List<User> getUsers() {
		String sql = "SELECT * " + 
	                 "  FROM users ";
		
		Object[] args = null;		
		List<User> userList = template.query(sql, args, MSU.EXAMPLE_BPRM);		
		return userList;
	}
	
	public List<User> getUsersOLD() {
		String sql = "SELECT id, first_name, last_name " + 
	                 "  FROM users ";
		Object[] args = null;
		List<Map<String, Object>> result = template.queryForList(sql, args);
		List<User> userList = new ArrayList<User>();
		
		for (Map<String, Object> map : result) {
			User user = new User();
			
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				String key = entry.getKey();
				String data = ((entry.getValue() == null) ? null : entry.getValue().toString());

				switch(key) {
				case "ID":					
					user.setId(Integer.valueOf(data));
					break;
				case "FIRST_NAME":
					user.setFirstName(data);
					break;
				case "LAST_NAME":
					user.setLastName(data);
					break;
				}
			}
			
			if (user.getId() != null) {
				userList.add(user);
			}
		}
		
		return userList;
	}

	public void addUser(User user) {
		String sql = "INSERT INTO USERS (first_name, last_name) " + 
                     "VALUES (?, ?)";
	
		Object[] args = {user.getFirstName(), user.getLastName()};
		template.update(sql, args);
	}
    
}

